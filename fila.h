#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define tam 15

typedef struct Monitor{
  char modelo[15];
  char resolucao[15];
  float preco;
}Monitor;

typedef struct Fila{
  Monitor fila[tam];
  int inicio, fim;
}Fila;

void iniciarFila(Fila *f){
  f->inicio = 0;
  f->fim = -1;
}

void insere(Fila *f, char modelo[], char resolucao[], float preco){
  Monitor newMonitor;
  strcpy(newMonitor.modelo, modelo);
  strcpy(newMonitor.resolucao, resolucao);
  newMonitor.preco = preco;
  f->fim++;
  f->fila[f->fim] = newMonitor;
  printf("Monitor cadastrado!\n");
}

void remove(Fila *f){

  f->inicio++;
  printf("Primeiro Monitor da fila removido!\n");
}

void exibe(Fila *f){
  printf("Primeiro Monitor da fila\n");
  printf("modelo do Monitor: %s\n", f->fila[f->inicio].modelo);
  printf("resolucao do Monitor: %s\n", f->fila[f->inicio].resolucao);
  printf("Preço do Monitor: %f\n", f->fila[f->inicio].preco);
}
